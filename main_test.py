import unittest

import main


class TestModularInverse(unittest.TestCase):

    def test_caesar_1(self):
        message = 'ALAMAKOTA'
        key = 1
        encrypted = main.encode_caesar(message, key)
        self.assertEqual('BMBNBLPUB', encrypted)
        decrypted = main.decode_caesar(encrypted, key)
        self.assertEqual(message, decrypted)

    def test_caesar_2(self):
        message = 'AZ'
        key = 10
        encrypted = main.encode_caesar(message, key)
        self.assertEqual('KJ', encrypted)
        decrypted = main.decode_caesar(encrypted, key)
        self.assertEqual(message, decrypted)

    def test_vigenere_1(self):
        message = 'ENCYKLOPEDIATECHNIKI'
        key = 'OKRES'
        encrypted = main.encode_vigenere(message, key)
        self.assertEqual('SXTCCZYGIVWKKIUVXZOA', encrypted)
        decrypted = main.decode_vigenere(encrypted, key)
        self.assertEqual(message, decrypted)

    def test_vigenere_2(self):
        message = 'TOJESTBARDZOTAJNYTEKST'
        key = 'TAJNE'
        encrypted = main.encode_vigenere(message, key)
        self.assertEqual('MOSRWMBJEHSOCNNGYCROLT', encrypted)
        decrypted = main.decode_vigenere(encrypted, key)
        self.assertEqual(message, decrypted)


if __name__ == '__main__':
    unittest.main()
