alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
            'V', 'W', 'X', 'Y', 'Z']


def next_char(char, key):
    encoded_char = alphabet[(alphabet.index(char) + key) % len(alphabet)]
    return encoded_char


def encode_caesar(message, key):
    return ''.join(list(map(lambda char: next_char(char, key), message)))


def decode_caesar(cipher, key):
    return ''.join(list(map(lambda char: next_char(char, -key), cipher)))


def build_key_stream(key, length):
    key_len = len(key)
    key_stream = key * (int(length / key_len) + 1)
    return key_stream[0:length]


def encode_vigenere(message, key):
    return ''.join([next_char(m_char, alphabet.index(k_char)) for (m_char, k_char) in
                    zip(message, build_key_stream(key, len(message)))])


def decode_vigenere(cipher, key):
    return ''.join([next_char(c_char, -alphabet.index(k_char)) for (c_char, k_char) in
                    zip(cipher, build_key_stream(key, len(cipher)))])


if __name__ == '__main__':
    try:
        operation = input("operation e - encrypt, d - decrypt: ")

        if operation == "e":
            message = input('message: ')
            key = input('key: ')
            print(f"Encoded text: {encode_vigenere(message, key)}")
        elif operation == "d":
            cipher = input('cipher: ')
            key = input('key: ')
            print(f"Decoded text: {decode_vigenere(cipher, key)}")
    except Exception as e:
        raise e
